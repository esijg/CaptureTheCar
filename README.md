![](https://img.itch.io/aW1hZ2UvNTM0MjAvMjM4MzA5LmdpZg==/315x250%23c/CMT%2B%2FF.gif)

https://johannesg.itch.io/capture-the-car-global-game-jam-2016-version

4 player vehicular reverse tag, with rockets. Will you manage to stay as "it" for 60 seconds?

Prototype made over the course of 48 hours during Global Game Jam 2016 over at Kollafoss Gamedev Residency, Iceland.

# Credits

- Andri Sæmundsson, Graphics
- Benóný Þór Björnsson, Code
- Brynjar Sæmundsson, Music
- Jóhannes Gunnar Þorsteinsson, Code/Sound Design
- Sölvi Mars, Graphics 